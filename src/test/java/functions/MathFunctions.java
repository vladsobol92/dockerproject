package functions;

import helpers.Logger;

public class MathFunctions {

    public static int addInt(int a, int b){
        Logger.log("addInt: "+a+" + "+b);
        return a+b;
    }

    public static int deduct(int a, int b){
        Logger.log("deduct: "+a+" - "+b);
        return a-b;
    }
}
