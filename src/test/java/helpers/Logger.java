package helpers;

import java.time.LocalTime;

public class Logger {

    public static <T> void log(T message) {
        System.out.println("[" + getCurrentTime() + "] [" + Thread.currentThread().getName() + "]" + message);
    }

    private static LocalTime getCurrentTime() {
        return java.time.LocalTime.now();
    }
}
