package tests;

import functions.MathFunctions;
import org.testng.Assert;
import org.testng.annotations.Test;

import static helpers.Logger.*;
import static helpers.Sleeper.*;


public class FirstTest extends BaseTest{
    @Test
    public void testNumberOne(){
        log("Running: testNumberOne");
        sleep(3000);
       int result =  MathFunctions.addInt(3,3);
       int expected = 6;
        Assert.assertEquals(expected, result, "FAILED");

    }


    @Test
    public void testNumberTwo(){
        log("testNumberTwo");
        sleep(3000);
        int result =  MathFunctions.addInt(3,2);
        int expected = 5;
        Assert.assertEquals(expected, result, "FAILED");

    }

    @Test
    public void testNumberThree(){
        log("testNumberThree");
        sleep(3000);
        int result =  MathFunctions.addInt(3,1);
        int expected = 4;
        Assert.assertEquals(expected, result, "FAILED");

    }


    @Test
    public void testNumberFour(){
        log("testNumberFour");
        sleep(3000);
        int result =  MathFunctions.deduct(7,2);
        int expected = 5;
        Assert.assertEquals(expected, result, "FAILED");

    }
}
