package tests;

import lombok.Getter;
import lombok.Setter;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import static helpers.Logger.*;
public class BaseTest {
    @Setter@Getter
    static String testProperty;

    @Parameters({"testProperty"})
    @BeforeSuite(alwaysRun = true)
   public void beforeSuite (@Optional("") String testProperty){
        setTestProperty(testProperty);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method){
        log("Starting: "+method.getName());
        log("This is testProperty: "+getTestProperty());
    }


    @AfterMethod(alwaysRun = true)
    public void afterMethod(Method method){
        log("Finished: "+method.getName());
    }

}
