# use Java image
FROM openjdk:8
# set propert to pass as argument
ENV testProperty default
# create a folder inside container
RUN mkdir -p /usr/src/dockerProject
# install maven inside container
RUN apt-get update \
&& apt-get install maven -y
# set containers working directorydocker ls
WORKDIR /usr/src/dockerProject
# copy all files to working directory
COPY . .
#CMD ["mvn", "test", "-DsuiteName=src/suite.xml", "-DtestProperty=${testProperty}"]
# run test command
CMD mvn test -DsuiteName=src/suite.xml -DtestProperty=${testProperty}


